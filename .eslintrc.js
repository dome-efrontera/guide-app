module.exports = {
  root: true,
  extends: ["eslint:recommended", "plugin:prettier/recommended"],
  rules: {
    curly: ["error", "all"],
    eqeqeq: ["error", "always", { null: "ignore" }],
    //"no-console": ["error", { allow: ["error"] }],
    "no-sequences": ["error", { allowInParentheses: false }],
    "no-template-curly-in-string": "error",
    "no-unused-expressions": "error",
    "prefer-template": "error",
    quotes: "off",
    "prettier/prettier": "error",
  },
  overrides: [
    {
      /* TypeScript */
      files: ["**/*.ts", "**/*.tsx"],
      parser: "@typescript-eslint/parser",
      plugins: ["react", "react-hooks", "@typescript-eslint"],
      extends: [
        "plugin:react/recommended",
        "plugin:react-hooks/recommended",
        "plugin:@typescript-eslint/recommended",
      ],
      parserOptions: {
        ecmaFeatures: {
          jsx: true,
        },
      },
      settings: {
        react: {
          version: "detect",
        },
      },
      rules: {
        "react/prop-types": "off",
        "react/react-in-jsx-scope": "off",
        "react-hooks/rules-of-hooks": "error",
        "react-hooks/exhaustive-deps": "error",

        "@typescript-eslint/array-type": ["error", { default: "array-simple" }],
        "@typescript-eslint/consistent-type-assertions": "error",
        "@typescript-eslint/explicit-function-return-type": [
          "off",
          { allowExpressions: true },
        ],
        "@typescript-eslint/explicit-module-boundary-types": "error",
        "@typescript-eslint/member-ordering": "error",
        "@typescript-eslint/no-empty-interface": [
          "error",
          { allowSingleExtends: false },
        ],
        "@typescript-eslint/no-explicit-any": [
          "error",
          { ignoreRestArgs: true },
        ],
        "@typescript-eslint/no-non-null-assertion": "error",
        "@typescript-eslint/no-use-before-define": [
          "error",
          { functions: false, classes: false },
        ],
        "@typescript-eslint/no-unused-vars": [
          "error",
          { args: "after-used", ignoreRestSiblings: true },
        ],
      },
    },
    {
      /* Javascript */
      files: ["**/*.js"],
      parser: "@babel/eslint-parser",
      env: {
        node: true,
        commonjs: true,
        es6: true,
      },
    },
    {
      /* Supongo que esto hará falta cuando configue cy */
      files: ["**/*.cy.spec.ts", "**/*.cy.spec.tsx"],
      rules: {
        "no-unused-expressions": "off",
      },
    },
  ],
};
