describe('Example', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('should have camera text', async () => {
    await expect(element(by.text('Camera'))).toExist();
  });

});
