import {AppRegistry} from 'react-native';
//import appName from './appConfig.js';
import App from './src/App';
import "react-native-reanimated";

AppRegistry.registerComponent('guideApp', () => App);
