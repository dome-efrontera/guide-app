import React from "react";
import { StyleSheet, View } from "react-native";

import "react-native-reanimated";

import { NativeRouter, Route } from "react-router-native";

import CameraQR from "components/CameraQR";
import CameraView from "components/CameraView";
import Hello from "components/Hello";
import FakeRequest from "components/FakeRequest";
import Index from "components/Index";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
});

const App: React.FC = () => {
  return (
    <NativeRouter>
      <View style={styles.container}>
        <Route exact path="/" component={Index} />
        <Route path="/cameraQr" component={CameraQR} />
        <Route path="/camera" component={CameraView} />
        <Route path="/hello" component={Hello} />
        <Route path="/fakeRequest" component={FakeRequest} />
      </View>
    </NativeRouter>
  );
};

export default App;
