import React from "react";
import { View, StyleSheet, Text } from "react-native";
import { getStatusBarHeight } from "react-native-status-bar-height";

const styles = StyleSheet.create({
  container: {
    backgroundColor: "blue",
    flexDirection: "row",
    paddingTop: getStatusBarHeight(true), //lo pongo a true porque no se está solapando con la statusBar en mi teléfono Android
  },
});

const AppBar: React.FC = () => {
  return (
    <View style={styles.container}>
      <Text>AppBar</Text>
    </View>
  );
};

export default AppBar;
