import React, { useEffect, useState } from "react";
import { StyleSheet, Text } from "react-native";
import { useCameraDevices, Camera } from "react-native-vision-camera";
import { useScanBarcodes, BarcodeFormat } from "vision-camera-code-scanner";
import useBackHandler from "../hooks/useBackHandler";

const styles = StyleSheet.create({
  barcodeTextURL: {
    color: "white",
    fontSize: 20,
    fontWeight: "bold",
  },
});

const CameraQR: React.FC = () => {
  useBackHandler();

  const [hasPermission, setHasPermission] = useState(false);

  const [frameProcessor, barcodes] = useScanBarcodes([BarcodeFormat.QR_CODE], {
    checkInverted: true,
  });

  const checkCameraPermission = async () => {
    const status = await Camera.getCameraPermissionStatus();

    if (status !== "authorized") {
      const res = await Camera.requestCameraPermission();
      setHasPermission(res === "authorized");
    } else {
      setHasPermission(true);
    }
  };

  useEffect(() => {
    checkCameraPermission();
  }, []);

  const devices = useCameraDevices();
  const device = devices.back;

  return (
    <>
      {device != null && hasPermission && (
        <>
          <Camera
            style={StyleSheet.absoluteFill}
            device={device}
            isActive={true}
            frameProcessor={frameProcessor}
            frameProcessorFps={5}
          />
          {barcodes.map((barcode, idx) => (
            <Text key={idx} style={styles.barcodeTextURL}>
              {barcode.displayValue}
            </Text>
          ))}
        </>
      )}
    </>
  );
};

export default CameraQR;
