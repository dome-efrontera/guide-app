import React, { useEffect, useState } from "react";
import { StyleSheet, Text, TouchableWithoutFeedback } from "react-native";
import { useCameraDevices, Camera } from "react-native-vision-camera";
import useBackHandler from "../hooks/useBackHandler";

const styles = StyleSheet.create({
  barcodeTextURL: {
    fontSize: 20,
    color: "white",
    fontWeight: "bold",
  },
  button: {
    position: "absolute",
    alignSelf: "center",
    color: "blue",
  },
});

const CameraView: React.FC = () => {
  useBackHandler();

  const [hasPermission, setHasPermission] = useState(false);

  const checkCameraPermission = async () => {
    const status = await Camera.getCameraPermissionStatus();
    console.log(status);
    if (status !== "authorized") {
      const res = await Camera.requestCameraPermission();
      console.log(res);
      setHasPermission(res === "authorized");
    } else {
      console.log("Ya tenia permiso");
      setHasPermission(true);
    }
  };

  useEffect(() => {
    checkCameraPermission();
  }, []);

  const devices = useCameraDevices();
  const device = devices.back;

  return (
    <>
      {device != null && hasPermission && (
        <>
          <Camera
            style={StyleSheet.absoluteFill}
            device={device}
            isActive={true}
          />
          <TouchableWithoutFeedback style={styles.button}>
            <Text>Boton</Text>
          </TouchableWithoutFeedback>
        </>
      )}
    </>
  );
};

export default CameraView;
