import React, { useEffect, useState } from "react";
import axios from "axios";
import { Server } from "miragejs";
import { StyleSheet, Text, View } from "react-native";

import useBackHandler from "../hooks/useBackHandler";
import { makeServer } from "../../test/server";

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
  },
  text: {
    color: "grey",
    fontSize: 20,
    fontWeight: "bold",
  },
});

declare let window: {
  server: Server;
};

if (window.server) {
  window.server.shutdown();
}

window.server = makeServer();

type Movie = {
  id: number;
  name: string;
  year: number;
};

const instance = axios.create({
  baseURL: "http://localhost:8081",
  timeout: 6000,
});

const FakeRequest: React.FC = () => {
  const [movies, setMovies] = useState<Movie[]>([]);

  useBackHandler();

  useEffect(() => {
    async function getMovies() {
      const response = await instance.get("/movies");
      setMovies(response.data);
    }
    getMovies();
  }, []);

  return (
    <View style={styles.container}>
      {movies &&
        movies.map((movie) => (
          <Text key={movie.id} style={styles.text}>
            {movie.name} ({movie.year})
          </Text>
        ))}
    </View>
  );
};

export default FakeRequest;
