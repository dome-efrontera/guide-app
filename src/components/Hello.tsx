import React from "react";
import { StyleSheet, Text, View } from "react-native";
import useBackHandler from "../hooks/useBackHandler";

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    backgroundColor: "black",
    flex: 1,
    justifyContent: "center",
  },
  text: {
    fontSize: 40,
    fontWeight: "bold",
  },
});

const Hello: React.FC = () => {
  useBackHandler();

  return (
    <View style={styles.container}>
      <Text style={styles.text}>HELLO WORLD</Text>
    </View>
  );
};

export default Hello;
