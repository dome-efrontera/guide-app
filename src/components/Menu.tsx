import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Link } from "react-router-native";

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
  },
  button: {
    backgroundColor: "grey",
    marginTop: 25,
    padding: 20,
  },
});

const Menu: React.FC = () => {
  return (
    <View style={styles.container}>
      <Link style={styles.button} to="/hello">
        <Text>Hello World</Text>
      </Link>

      <Link style={styles.button} to="/fakeRequest">
        <Text>Fake request</Text>
      </Link>

      <Link style={styles.button} to="/camera">
        <Text>Camera</Text>
      </Link>

      <Link style={styles.button} to="/cameraQr">
        <Text>Camera QR Reader</Text>
      </Link>
    </View>
  );
};

export default Menu;
