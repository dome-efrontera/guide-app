import { useEffect } from "react";
import { BackHandler } from "react-native";
import { useHistory } from "react-router-native";

const useBackHandler = (): void => {
  const history = useHistory();

  useEffect(() => {
    const backAction = () => {
      console.log("backButton");
      history.goBack();
      return true;
    };

    //TODO: Si es la pantalla principal, permitir salir de la app.

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction,
    );

    return () => {
      console.log("unmount");
      backHandler.remove();
    };
  }, [history]);
};

export default useBackHandler;
