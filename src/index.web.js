import { AppRegistry } from 'react-native';
import appName from '../appConfig.js';
import App from './App';

AppRegistry.registerComponent(appName, () => App);

AppRegistry.runApplication(appName, {
  initialProps: {},
  rootTag: document.getElementById('app-root'),
});
