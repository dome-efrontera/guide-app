/* eslint-env browser */
import { createServer, Response } from "miragejs";

if (window.Cypress) {
  const domains = ["http://localhost:8080"];
  const methods = ["get", "put", "patch", "post", "delete"];

  createServer({
    environment: "test",
    routes() {
      for (const domain of domains) {
        for (const method of methods) {
          this[method](`${domain}/*`, async (schema, request) => {
            const [status, headers, body] = await window.handleFromCypress(
              request,
            );
            return new Response(status, headers, body);
          });
        }
      }

      this.passthrough("http://localhost:8080/**");
    },
  });
}