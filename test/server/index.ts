import { createServer, Server } from "miragejs";

export function makeServer(): Server {
  return createServer({
    routes() {
      this.urlPrefix = "http://localhost:8081";
      this.namespace = "/";

      this.get("/movies", () => {
        return [
          { id: 1, name: "Inception", year: 2010 },
          { id: 2, name: "Interstellar", year: 2014 },
          { id: 3, name: "Dunkirk", year: 2017 },
          { id: 4, name: "Matrix", year: 2001 },
        ];
      });
    },
  });
}
