const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

const mode = process.env.NODE_ENV || 'development';

function bool(value) {
  return value === true || value === "true";
}

module.exports = function (env = {}) {
  const cypress = bool(env.cy);

  return {
    entry: [
      cypress && path.resolve(__dirname, "test", "server", "CyApiServer.js"),
      path.resolve(__dirname, 'src', 'index.web.js')].filter(Boolean),
    mode: mode,
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
          },
        },
      ],
    },
    output: {
      filename: 'main.js',
      path: path.resolve(__dirname, 'dist'),
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: path.resolve(__dirname, 'src', 'web', 'index.html'),
      }),
      new webpack.HotModuleReplacementPlugin(),
    ],
    resolve: {
      extensions: [
        '.web.tsx',
        '.web.ts',
        '.tsx',
        '.ts',
        '.web.jsx',
        '.web.js',
        '.jsx',
        '.js',
      ], // read files in fillowing order
      alias: {
        'react-native$': 'react-native-web',
      },
    },
  }
};
